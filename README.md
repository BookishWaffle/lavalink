# Lavalink
**What is Lavalink?** Think of Lavalink as a sending node. This service can be ran from anywhere.

# Requirements
Here are some requirements to run Lavalink:
- Java 11 LTS or greater required.
- OpenJDK or Zulu running on Linux AMD64 is officially supported.

Lavalink is supported on several platforms. You just need a sufficient amount of memory and a good processor.

# Lavalink Client Libraries
There are a number of Lavalink client libraries. Just find one that you like or you're able to make your own.

# Shell File
I'll provide a shell file to run so that it's easier to use this configuration.

# Acknowledgements
Lavalink was made by [Frederikam](https://github.com/Frederikam).